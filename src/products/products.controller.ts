import { HttpService } from '@nestjs/axios';
import { Body, Controller, Delete, Get, HttpException, Patch, Post } from '@nestjs/common';
import { ProductDto } from './dto/product.dto';

@Controller('products')
export class ProductsController {
    constructor(
        private readonly httpService: HttpService
    ){}

    @Get('product')
    async getProduct(@Body ('product')product: string){
        await this.httpService.axiosRef.patch(
            'http://localhost:3001/users/registerUser', {
                    product
                }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

//CREATE

    @Post('addProduct')
    async addProduct(@Body() productDto: ProductDto){
        await this.httpService.axiosRef.post(
            'http://localhost:3001/users/registerUser', {
                    id: productDto.id,
                    name: productDto.name,
                    price: productDto.price,
                    image: productDto.image,
                    description: productDto.description,
                    stock: productDto.stock
                }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

//UPDATE

    @Patch('updateProduct')
    async updateProduct(@Body()  productDto: ProductDto){
        await this.httpService.axiosRef.patch(
            'http://localhost:3001/users/registerUser', {
                    id: productDto.id,
                    name: productDto.name,
                    price: productDto.price,
                    image: productDto.image,
                    description: productDto.description,
                    stock: productDto.stock
                }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    @Patch('updateProductStock')
    async updateProductStock(
      @Body('stock') stock: number,
      @Body('name') name: string
      ){
        await this.httpService.axiosRef.patch(
            'http://localhost:3001/users/registerUser', {
                    name,
                    stock
                }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    @Patch('updateProductPrice')
    async updateProductPrice(
      @Body('price') price: number,
      @Body('name') name: string
      ){
        await this.httpService.axiosRef.patch(
            'http://localhost:3001/users/registerUser', {
                    name,
                    price
                }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

//DELETE

    @Delete('deleteProduct')
    async deleteProduct(@Body() productDto: ProductDto){
        await this.httpService.axiosRef.patch(
            'http://localhost:3001/users/registerUser', {
                    id: productDto.id,
                    name: productDto.name,
                    price: productDto.price,
                    image: productDto.image,
                    description: productDto.description,
                    stock: productDto.stock
                }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }
}
