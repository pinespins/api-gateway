export class  ProductDto {
    id: number
    name: string;
    price: number;
    image: string;
    description: string;
    stock: number;
  }
  