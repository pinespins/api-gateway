/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { HttpService } from '@nestjs/axios';
import { Body, Controller, Get, HttpException, Patch, Post, Req } from '@nestjs/common';
import { ChangePasswordDto } from 'src/dto/change_password.dto';
import { CreateUserDto } from 'src/dto/create_user.dto';
import { LogInDto } from 'src/dto/log_in.dto';
import { OwnerDto } from 'src/dto/owner.dto';
import { CreateProfileDto } from 'src/dto/profile.dto';
import { UpdatePasswordDto } from 'src/dto/update_password.dto';
import { VerifyEmailDto } from 'src/dto/verify_email.dto';

@Controller('users')
export class LoginController {
    constructor(
        private readonly httpService: HttpService
    ) { }

    //CREATE
    @Post('registerUser')
    async registerUser(@Body() userdto: CreateUserDto) {
        console.log(userdto)
        await this.httpService.axiosRef.post(
            'http://localhost:3002/users/registerUser', {
                    name: userdto.name,
                    lastName: userdto.lastName,
                    email: userdto.email,
                    password: userdto.password
                }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    @Patch('registerProfile')
    async registerProfile(@Body() profileDto: CreateProfileDto) {
        return await this.httpService.axiosRef.patch(
            'http://localhost:3002/users/registerProfile', {
                data: {
                    name: profileDto.name,
                    lastName: profileDto.lastName,
                    address: profileDto.address,
                    city: profileDto.city,
                    phone: profileDto.phone,
                    email: profileDto.email
                }
        }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    @Patch('verifyEmail')
    async verifiyEmail(@Body() verifyDto: VerifyEmailDto) {
        return await this.httpService.axiosRef.patch(
            'http://localhost:3002/users/verifyEmail', {
                data: {
                    email: verifyDto.email,
                    verificationCode: verifyDto.verificationCode
                }
        }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    //READ
    @Post('login')
    async logIn(@Body("email") email: string, @Body("password") password: string) {
        console.log(email);
        console.log(password);
        return await this.httpService.axiosRef.post(
            'http://localhost:3002/auth/login', {
                email,
                password
        }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            //console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    @Get('exists')
    async exists(@Body() ownerDto: OwnerDto) {
        return await this.httpService.axiosRef.get(
            'http://localhost:3002/users/exists', {
                data: {
                    owner: ownerDto.owner
                }
        }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }


    //UPDATE

    @Patch('changePasswordRequest')
    async ChangePassword(@Body() userDto: UpdatePasswordDto) {
        return await this.httpService.axiosRef.get(
            'http://localhost:3002/users/changePasswordRequest', {
                data: {
                    email: userDto.email,
                    password: userDto.password
                }
        }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    @Patch('changePassword')
    async changePassword(@Body() passwordDto: ChangePasswordDto) {
        return await this.httpService.axiosRef.patch(
            'http://localhost:3002/users/changePassword', {
                data: {
                    email: passwordDto.email,
                    newPassword: passwordDto.newPassword,
                    codigo: passwordDto.newPassword
                }
        }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

    @Patch('UpdateProfile')
    async changeProfileInfo(
        @Body() profileDto: CreateProfileDto,
    ) {
        return await this.httpService.axiosRef.patch(
            'http://localhost:3002/users/UpdateProfile', {
                data: {
                    name: profileDto.name,
                    lastName: profileDto.lastName,
                    address: profileDto.address,
                    city: profileDto.city,
                    phone: profileDto.phone,
                    email: profileDto.email
                }
        }
        ).then(function (response) {
            console.log(response.data);
            console.log(response.status);
            console.log(response.statusText);
            console.log(response.headers);
            console.log(response.config);
            return response.data
          }).catch(function (error) {
            console.log(error.toJSON());
            console.log('Error', error.toJSON().status);
            console.log('Mensaje', error.response.data.message);
            throw new HttpException(error.response.data.message, Number(error.toJSON().status))
          });
    }

}
