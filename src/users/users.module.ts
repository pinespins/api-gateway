import { Module } from '@nestjs/common';
import { LoginController } from './users.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  controllers: [LoginController]
})
export class LoginModule {}
