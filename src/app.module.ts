import { Module } from '@nestjs/common';
import { LoginModule } from './users/users.module';
import { ProductsModule } from './products/products.module';
import { PaymentsModule } from './payments/payments.module';


@Module({
  imports: [LoginModule, PaymentsModule, ProductsModule],

})
export class AppModule {}
