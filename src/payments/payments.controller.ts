/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { HttpService } from '@nestjs/axios';
import { Body, Controller, Get, HttpException, HttpStatus, Post, Redirect, Req } from '@nestjs/common';

@Controller('payments')
export class PaymentsController {
  constructor(private readonly httpService: HttpService) {}

  //CREATE
  @Post('create')
  async create(@Body() body, @Req() request) {
    const { sessionId, amount, host, protocol } = body;
    console.log(body);
    const response = await this.httpService.axiosRef
      .post('http://localhost:3005/transbank/create', {
        sessionId: sessionId,
        amount: amount,
        protocol: request.protocol,
        host: request.get('host'),
      });
      //.then(function (response) {
      //  console.log(response.data);
      //  console.log(response.status);
      //  console.log(response.statusText);
      //  console.log(response.headers);
      //  console.log(response.config);
      //  return response.data;
      //})
      //.catch(function (error) {
      //  console.log(error.toJSON());
      //  console.log('Error', error.toJSON().status);
      //  console.log('Mensaje', error.response.data.message);
      //  throw new HttpException(
      //    error.response.data.message,
      //    Number(error.toJSON().status),
      //  );
      //});
    
    return response.data;
  }

  //COMMIT
  @Get('commit')
  @Redirect()
  async commit(@Req() request) {
    const payload = {
      method: request.method,
      query: request.query,
      body: request.body,
    };
    console.log(payload);
    const response = await this.httpService.axiosRef.post('http://localhost:3005/transbank/commit', { payload })

    const success = response.data.commitResponse.response_code === 0 ? true: false;
      const url = `http://localhost:3000/compra_terminada?success=${success}`
    return {
      statusCode: HttpStatus.FOUND, url: url
    }
  }
}
