import { IsNotEmpty } from "class-validator";

export class CreateProfileDto {
    @IsNotEmpty()
    name: string;
    @IsNotEmpty()
    lastName: string;
    @IsNotEmpty()
    address: string;
    @IsNotEmpty()
    city: string;
    @IsNotEmpty()
    phone: string;
    @IsNotEmpty()
    email: string;
  }
  